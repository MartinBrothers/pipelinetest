#!/bin/bash

set -eu

DIRNAME="embedded"

DIRECTORIES=$(find "${DIRNAME}" -name "triggerfile" -exec dirname {} \;)
echo $DIRECTORIES

for DIRECTORY in $DIRECTORIES
do
	echo "${DIRECTORY}"
	PROJECT_NAME=$(echo "${DIRECTORY}" | sed 's|.*/||')
	echo "${PROJECT_NAME}"
	sed "s|%%%PROJECT_NAME%%%|${PROJECT_NAME}|g" "${DIRNAME}/embedded_template-ci.yml" > "${DIRECTORY}/.gitlab-ci.yml"
	DIR=$(dirname "${DIRECTORY}")
	sed -i "s|%%%PROJECT_PATH%%%|${DIR}|g" "${DIRECTORY}/.gitlab-ci.yml"
done
